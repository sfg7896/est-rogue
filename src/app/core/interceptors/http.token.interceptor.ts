import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(
    private tokenService: TokenService,
    private router: Router
    ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.indexOf('/login') > 0) {
      return next.handle(req);
    }
    if (req.url.indexOf('/register') > 0) {
      return next.handle(req);
    }
    const token = this.tokenService.getToken();
    if (token) {
      return next.handle(
        req.clone({
          headers: req.headers.append('Authorization', 'Bearer ' + token)
        })
      ).pipe(
        catchError((error: HttpErrorResponse) => {
          console.log(error);
          let errMsg = '';
          // Client Side Error
          if (error.error instanceof ErrorEvent) {
            errMsg = `Error: ${error.error.message}`;
          } else {  // Server Side Error
            errMsg = `Error Code: ${error.status},  Message: ${error.message}`;
          }
          if (error.error.err.errorType === '003') {
            this.tokenService.destroyToken();
            this.router.navigateByUrl('login');
          }
          return throwError(errMsg);
        })
      );
    }
    return next.handle(req);
  }
}
