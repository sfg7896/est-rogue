import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TokenService } from './services/token.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from './interceptors/http.token.interceptor';
import { HttpHeaderInterceptor } from './interceptors/http.header.interceptor';
import { CheckTokenService } from './services/check-token.service';
import { JwtHelperService, JWT_OPTIONS  } from '@auth0/angular-jwt';
import { ApiService } from './services/api.service';
import { LoggedCheckService } from './services/logged-check.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpHeaderInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    TokenService,
    CheckTokenService,
    ApiService,
    LoggedCheckService
  ]
})
export class CoreModule { }
