import { TestBed } from '@angular/core/testing';

import { LoggedCheckService } from './logged-check.service';

describe('LoggedCheckService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoggedCheckService = TestBed.get(LoggedCheckService);
    expect(service).toBeTruthy();
  });
});
