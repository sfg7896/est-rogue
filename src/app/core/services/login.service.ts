import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { Credential } from './../models/credential.model';

@Injectable()
export class LoginService {
  constructor(
    private tokenService: TokenService,
    private http: HttpClient,
  ) { }

  login(credentials: Credential): Observable<any> {
    return this.http.post<{token: string}>(`${environment.api_url}/login`, credentials)
    .pipe(map(
      data => {
        this.tokenService.saveToken(data.token);
        return data;
      }
    ));
  }
}
