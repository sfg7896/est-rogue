import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from './../models/user.model';

@Injectable()
export class UsersService {

  constructor(
    private apiService: ApiService, private http: HttpClient
  ) { }

  getUsers(): Observable<Array<User>> {
    return this.apiService.get('/users');
  }
}
