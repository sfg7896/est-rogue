import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenService } from './token.service';

@Injectable()
export class CheckTokenService implements CanActivate {

  constructor(
    private router: Router,
    private tokenService: TokenService
  ) {
  }

  canActivate(): boolean {
    if (!this.tokenService.isAuthenticated()) {
      this.router.navigate(['authorize/login']);
      return false;
    }
    return true;
  }
}
