import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedCheckService implements CanActivate {

  constructor(
    private router: Router,
    private tokenService: TokenService
  ) { }

  canActivate(): boolean {
    if (this.tokenService.isAuthenticated()) {
      this.router.navigate(['users']);
      return false;
    }
    return true;
  }
}
