import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class TokenService {

  constructor(
    private jwtHelper: JwtHelperService
  ) { }

  getToken(): string {
    return localStorage.token;
  }

  saveToken(token: string) {
    localStorage.token = token;
  }

  destroyToken() {
    localStorage.removeItem('token');
  }


  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

}
