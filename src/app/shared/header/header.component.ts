import { Component, OnInit } from '@angular/core';
import { TokenService } from './../../core/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(
    private tokenService: TokenService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  checkLoggedIn(): boolean {
    if (this.tokenService.isAuthenticated()) {
      return true;
    }
    return false;
  }

  logout() {
    this.router.navigate(['authorize/login']);
    this.tokenService.destroyToken();
  }
}
