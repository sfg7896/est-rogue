import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../../core/services/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Credential } from './../../../core/models/credential.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  get formGetter() { return this.loginForm.controls; }

  login() {
    const payload: Credential = this.loginForm.value;
    this.loginService.login(payload).subscribe(
      data => {
        this.router.navigateByUrl('/');
      },
      err => {
        throw err;
      }
    );
  }
}
