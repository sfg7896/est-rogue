import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from '../..//modules/authorize/login/login.component';
import { RegisterComponent } from '../../modules/authorize/register/register.component';
import { LoggedCheckService } from '../../core/services/logged-check.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedCheckService]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [LoggedCheckService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorizeRoutingModule { }
