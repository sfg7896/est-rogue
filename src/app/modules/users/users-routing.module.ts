import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListUsersComponent } from '../../modules/users/list-users/list-users.component';
import { CheckTokenService } from '../../core/services/check-token.service';

const routes: Routes = [
  {
    path: '',
    component: ListUsersComponent,
    canActivate: [CheckTokenService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
