import { Component, OnInit, Input } from '@angular/core';
import { User } from './../../../core/models/user.model';

@Component({
  selector: 'app-list-user-dumb',
  templateUrl: './list-user-dumb.component.html',
  styleUrls: ['./list-user-dumb.component.css']
})
export class ListUserDumbComponent implements OnInit {
  @Input() users: Array<User> = [];
  currentPage = 1;
  constructor() { }

  ngOnInit() {
  }

}
