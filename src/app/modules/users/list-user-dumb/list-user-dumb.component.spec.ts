import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUserDumbComponent } from './list-user-dumb.component';

describe('ListUserDumbComponent', () => {
  let component: ListUserDumbComponent;
  let fixture: ComponentFixture<ListUserDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUserDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUserDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
