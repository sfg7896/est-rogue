import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsersComponent } from './list-users/list-users.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListUserDumbComponent } from './list-user-dumb/list-user-dumb.component';
import { UsersService } from './../../core/services/users.service';
import { UsersRoutingModule } from './users-routing.module';



@NgModule({
  declarations: [
    ListUsersComponent,
    ListUserDumbComponent
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    UsersRoutingModule
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
