import { Component, OnInit } from '@angular/core';
import { User } from './../../../core/models/user.model';
import { UsersService } from './../../../core/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  users: Array<User> = [];
  constructor(private userService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(
      data => {
        data.forEach(user => {
          this.users.push(user);
        });
      }, err => {
        throw err;
      }
    );
  }
}
